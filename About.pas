unit About;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ShellApi;

type
  TfmAbout = class(TForm)
    Panel1: TPanel;
    lblwww: TLabel;
    lblCopyright: TLabel;
    lblSpacePlanner: TLabel;
    lblVersion: TLabel;
    lblContribute: TLabel;
    lblErr: TLabel;
    mCopyright: TMemo;
    btnDismiss: TButton;
    procedure btnDismissClick(Sender: TObject);
    procedure lblwwwClick(Sender: TObject);
    procedure lblContributeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAbout: TfmAbout;

implementation

{$R *.dfm}

procedure TfmAbout.btnDismissClick(Sender: TObject);
begin
  Close;
end;

procedure TfmAbout.lblContributeClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','www.dhsoftware.com.au/contribute.htm',nil,nil, SW_SHOWNORMAL);
end;

procedure TfmAbout.lblwwwClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','www.dhsoftware.com.au',nil,nil, SW_SHOWNORMAL);
end;

end.
