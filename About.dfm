object fmAbout: TfmAbout
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'fmAbout'
  ClientHeight = 282
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 282
    Align = alClient
    BorderStyle = bsSingle
    Color = clWindow
    ParentBackground = False
    TabOrder = 0
    object lblwww: TLabel
      Left = 591
      Top = 8
      Width = 144
      Height = 16
      Cursor = crHandPoint
      Alignment = taRightJustify
      Caption = 'www.dhsoftware.com.au'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      OnClick = lblwwwClick
    end
    object lblCopyright: TLabel
      Left = 15
      Top = 26
      Width = 159
      Height = 13
      Caption = 'Copyright David Henderson 2018'
    end
    object lblSpacePlanner: TLabel
      Left = 15
      Top = 4
      Width = 127
      Height = 16
      Caption = 'Text Replace Macro'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblVersion: TLabel
      Left = 157
      Top = 4
      Width = 35
      Height = 16
      Caption = 'v1.1.0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblContribute: TLabel
      Left = 15
      Top = 234
      Width = 234
      Height = 26
      Cursor = crHandPoint
      Hint = 'www.dhsoftware.com.au/contribute.htm'
      Caption = 
        'Contribute towards the development of this and other DataCAD mac' +
        'ros'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      WordWrap = True
      OnClick = lblContributeClick
    end
    object lblErr: TLabel
      Left = 19
      Top = 192
      Width = 3
      Height = 13
      Color = clRed
      ParentColor = False
      ParentShowHint = False
      ShowHint = False
    end
    object mCopyright: TMemo
      Left = 14
      Top = 45
      Width = 728
      Height = 169
      TabStop = False
      BorderStyle = bsNone
      Lines.Strings = (
        ''
        
          'This software is distributed free of charge and WITHOUT WARRANTY' +
          '. You may distribute it to others provided that you distribute t' +
          'he complete '
        
          'unaltered installation file provided by me at the dhsoftware.com' +
          '.au web site, and that you do so free of charge (This includes n' +
          'ot  charging for '
        
          'distribution media and not charging for any accompanying softwar' +
          'e that is on the same media or contained in the same download or' +
          ' distribution file). '
        
          'If you wish to make any charge at all you need to obtain specifi' +
          'c permission from me.'
        ''
        
          'Whilst it is free (or because of this) I would like and expect t' +
          'hat if you can think of any improvements or spot any bugs (or ev' +
          'en spelling or formatting '
        
          'errors in the documentation) that you would let me know.  Your f' +
          'eedback will help with future development of the macro.'
        ''
        
          'Because this macro is free you are not required to pay for it.  ' +
          'But if you find it useful then a financial contribution towards ' +
          'the cost of its development '
        
          'and distribution is expected and would be appreciated. Contribut' +
          'ions can be made using the link at the left below.')
      ReadOnly = True
      TabOrder = 0
    end
    object btnDismiss: TButton
      Left = 560
      Top = 232
      Width = 182
      Height = 28
      Caption = 'Dismiss'
      TabOrder = 1
      OnClick = btnDismissClick
    end
  end
end
