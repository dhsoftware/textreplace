unit ReplaceForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, UInterfaces, URecords, Lyr, Vcl.ExtCtrls,
  StrUtils, UConstants, System.UITypes, Vcl.Menus, About, Settings, ShellApi,
  CommonStuff, SmartEntProcedure;

type
  TfReplace = class(TForm)
    edLookFor: TEdit;
    edReplaceWith: TEdit;
    lblSearchFor: TLabel;
    lblReplaceWith: TLabel;
    ListBox1: TListBox;
    rbON: TRadioButton;
    rbALL: TRadioButton;
    btnProcess: TButton;
    rbSelect: TRadioButton;
    rbCurrent: TRadioButton;
    lblLayers: TLabel;
    cbSorted: TCheckBox;
    cbWholeWords: TCheckBox;
    cbCaseSensitive: TCheckBox;
    cbConfirmEach: TCheckBox;
    cbWholeEnts: TCheckBox;
    btnReplaceExit: TButton;
    MainMenu1: TMainMenu;
    About1: TMenuItem;
    btnExit: TButton;
    Hel1: TMenuItem;
    procedure rbCurrentClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbSortedClick(Sender: TObject);
    procedure rbALLClick(Sender: TObject);
    procedure rbONClick(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure btnProcessClick(Sender: TObject);
    procedure btnReplaceExitClick(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure rbSelectClick(Sender: TObject);
    procedure cbWholeWordsClick(Sender: TObject);
    procedure cbWholeEntsClick(Sender: TObject);
    procedure cbCaseSensitiveClick(Sender: TObject);
    procedure cbConfirmEachClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure Hel1Click(Sender: TObject);
  private
  public
  end;


var
  fReplace: TfReplace;
  justStarting : boolean;
  mtxtstr : string;


implementation

{$R *.dfm}

 procedure SmartEntProc (ent: entity; var p: pipe_type; var xform: ModMat); stdcall;
 begin
   if ent.enttype = enttxt then begin
     if mtxtstr > '' then
       mtxtstr := mtxtstr + ' ';
     mtxtstr := mtxtstr + string(ent.txtstr);
   end;
 end;



function LyrEqual (const lyr1, lyr2 : lyraddr) : boolean;
BEGIN
  result := (lyr1.page = lyr2.page) and (lyr1.ofs = lyr2.ofs);
END;

PROCEDURE SelectedLyr (lyr : lyraddr; selected : boolean);
VAR
  atr : attrib;
BEGIN
  if justStarting then exit;
  if atr_lyrfind (lyr, 'dhTRSelected', atr) then begin
    if selected then
      exit
    else
      atr_dellyr (lyr, atr)
  end
  else if selected then begin
    atr_init (atr, atr_int);
    atr.name := 'dhTRSelected';
    atr.int := 0;
    atr_add2lyr (lyr, atr);
  end;
END;

FUNCTION IsSelectedLyr (lyr : lyraddr) : boolean;
VAR
  atr : attrib;
BEGIN
  result := atr_lyrfind (lyr, 'dhTRSelected', atr);
END;

procedure PopulateLyrList;
var
  MyLyr : TLyr;
  lyr     : lyraddr;
  lyrname : shortstring;
  ndx : integer;
  rlyr : Rlayer;
begin
  if justStarting then exit;
  lyr := lyr_first;
  repeat
    MyLyr := TLyr.Create(lyr);
    getlyrname (lyr, lyrname);
    lyr_get (lyr, rlyr);
    if rlyr.group <> -1 then
      lyrname := lyrname + ' (LOCKED)';
    ndx := fReplace.ListBox1.Items.AddObject(string(lyrname), MyLyr);
    if fReplace.rbCurrent.Checked then
      fReplace.ListBox1.Selected[ndx] := LyrEqual (lyr, GetLyrCurr)
    else if fReplace.rbON.Checked then
      fReplace.ListBox1.Selected[ndx] := Lyr_ison (lyr)
    else if fReplace.rbALL.Checked then
      fReplace.ListBox1.Selected[ndx] := true
    else if fReplace.rbSelect.Checked then
      fReplace.ListBox1.Selected[ndx] := IsSelectedLyr (lyr)
    else
      fReplace.ListBox1.Selected[ndx] := false;
    lyr := lyr_next(lyr);
  until isnil (lyr);
end;

procedure SaveSelected;
var
  i : integer;
begin
  if justStarting then exit;
  if fReplace.rbSelect.checked then begin
    // save existing selected layers
    for i := 0 to fReplace.ListBox1.Count-1 do
      if fReplace.ListBox1.Selected[i] then
        SelectedLyr (TLyr(fReplace.ListBox1.Items.Objects[i]).Addr, true)
      else
        SelectedLyr (TLyr(fReplace.ListBox1.Items.Objects[i]).Addr, false);
  end;
end;

var
  cancelled, doLocked, skipLocked : boolean;
  count, entcount, mtxtcount : word;

function ReplaceText (var ent : entity) : boolean;
var
  ndx, res : integer;
  TargetStr, SearchStr : string;
  validmatch, changedone : boolean;
  mymsg : string;
  lyrname : shortstring;
  updatedEnt : entity;
  MyCount : word;
  isLocked : boolean;
  rlyr : RLayer;
  replaceAll, confirmed : boolean;
begin
  MyCount := 0;
  result := true;
  changedone := false;
  confirmed := false;
  if fReplace.cbCaseSensitive.Checked then begin
    TargetStr := string (ent.txtstr);
    SearchStr := fReplace.edLookFor.Text;
  end
  else begin
    TargetStr := UpperCase (string(ent.txtstr));
    SearchStr := UpperCase (fReplace.edLookFor.Text);
  end;
  if fReplace.cbWholeEnts.Checked then begin
    if length(TargetStr) <> length(SearchStr) then
      exit;
  end;
  ndx := 1;
  replaceAll := (SearchStr = '');
  repeat
    if replaceAll then
      validmatch := true
    else begin
      ndx := pos (SearchStr, TargetStr, ndx);
      validmatch := (ndx > 0);
    end;
    if validmatch then begin
      if (not replaceAll) and fReplace.cbWholeWords.Checked then begin
        if ndx > 1 then
          validmatch := (ent.txtstr[ndx-1] = ' ');
        if ndx < (length(ent.txtstr) - length(fReplace.edLookfor.Text) + 1) then
          if ent.txtstr[ndx + length(fReplace.edLookfor.Text)] <> ' ' then
            validmatch := false;
      end;

      if validmatch and fReplace.cbConfirmEach.Checked and not confirmed then begin
        getlyrname (ent.lyr, lyrname);
        mymsg := Msgs[14] + sLineBreak + sLineBreak + Msgs[15] + sLineBreak +
                   string(lyrname) + sLineBreak + sLineBreak +
                   Msgs[16] + sLineBreak + string(ent.txtstr);
        res := messagedlg (mymsg, mtConfirmation, [mbYes, mbNo, mbCancel], 0);
        if res = mrCancel then begin
          validmatch := false;
          cancelled := true;
        end
        else
          validmatch := (res = mrYes);
        if validmatch then
          confirmed := true
        else
          exit;
      end;

      if validmatch then begin
        if ReplaceAll then
          ent.txtstr := '';
        if length(ent.txtstr) - length(fReplace.edLookFor.Text)
            + length(fReplace.edReplaceWith.Text) > 255 then begin
          result := false;
          exit;
        end
        else begin
          ent_draw (ent, drmode_black);
          if ReplaceAll then begin
            ent.txtstr := shortstring(fReplace.edReplaceWith.Text);
            ndx := 0;
            MyCount := MyCount + 1;
            changedone := true;
          end
          else begin
            delete (ent.txtstr, ndx, length(fReplace.edLookFor.Text));
            insert (shortstring(fReplace.edReplaceWith.Text), ent.txtstr, ndx);
            delete (TargetStr, ndx, length(SearchStr));
            insert (fReplace.edReplaceWith.Text, TargetStr, ndx);
            ndx := ndx + length(fReplace.edReplaceWith.Text);
            MyCount := MyCount + 1;
            changedone := true;
          end;
        end;
      end
      else
        ndx := ndx+1;
    end;
  until cancelled or (ndx < 1);

  if changedone then begin
    if doLocked then
      ent_updates (ent)
    else
      ent_update (ent);

    if ent_get (updatedEnt, ent.addr) then begin
      if ent.txtstr <> updatedEnt.txtstr then begin
        lyr_get (ent.lyr, rlyr);
        isLocked :=  rlyr.group <> -1;

        if isLocked and skiplocked then begin
          ent_draw (updatedEnt, drmode_white);
          exit;
        end;
        getlyrname (ent.lyr, lyrname);
        if isLocked and (not doLocked) then begin
          MyMsg := Msgs[30] + sLineBreak +sLineBreak +         //Failed to update the following entity:
                    Msgs[15] + sLineBreak + string(lyrname) + sLineBreak + sLineBreak +
                    Msgs[16] + sLineBreak + string(updatedEnt.txtstr) + sLineBreak + sLineBreak +
                    Msgs[28] + sLineBreak +  //Perhaps the layer is locked:
                    Msgs[29];          //Do you want to update entities on locked layers?
          res := messagedlg (MyMsg, mtWarning, [mbYes, mbNo, mbCancel], 0);
          if res = mrCancel then begin
            Cancelled := true;
            ent := updatedEnt;
            MyCount := 0;
          end
          else if res = mrYes then begin
            doLocked := true;
            ent_updates (ent);
          end
          else begin
            skipLocked := true;
            MyCount := 0;
          end;
        end;
        if doLocked or (not isLocked) then begin
          if ent_get (updatedEnt, ent.addr) then begin
            if ent.txtstr <> updatedEnt.txtstr then begin
              MyMsg := Msgs[30] + sLineBreak + sLineBreak +         //Failed to update the following entity:
                        Msgs[15] + sLineBreak + string(lyrname) + sLineBreak + sLineBreak +
                        Msgs[16] + sLineBreak + string(updatedEnt.txtstr);
              MessageDlg (MyMsg, mtError, [mbOK], 0);
              MyCount := 0;
              ent := updatedEnt;
            end;
          end;
        end;

      end;
    end;
    ent_draw (ent, drmode_white);
  end;
  if MyCount > 0 then begin
    Count := Count + MyCount;
    entcount := entcount + 1;
  end;
end;

function doReplace (var mode : mode_type) : boolean;
var
  addr : entaddr;
  ent : entity;
  pipe : pipe_type;
  mat : modmat;
  SrchStr : string;
  match : boolean;
begin
  mode_enttype (mode, enttxt);
  mode_enttype (mode, DCPlainTextObject);
  mode_enttype (mode, DCRTFTextObject);
  addr := ent_first (mode);
  result := true;
  while ent_get (ent, addr) and (not cancelled) do begin
    addr := ent_next (ent, mode);
    if ent.enttype = enttxt then begin
      if not ReplaceText (ent) then
        result := false;
    end
    else begin
      mtxtstr := '';
      SmartEntity2RegularEntities (ent, pipe, mat, SmartEntProc, true, true);
      messagedlg (mtxtstr, TMsgDlgType.mtInformation, [mbOK], 0);
      if not fReplace.cbCaseSensitive.Checked then begin
        mtxtstr := UpperCase (mtxtstr);
        SrchStr := UpperCase (fReplace.edLookFor.Text);
      end;
      if fReplace.cbWholeEnts.Checked and (length (mtxtstr) <> Length(SrchStr )) then
        match := false
      else
        match := mtxtstr.Contains(SrchStr);

      if match then
        mtxtcount := mtxtcount + 1;
    end;
  end;
end;

procedure ProcessReplace (var msg : string);
var
  i : integer;
  mode : mode_type;
  result : boolean;
begin
  SaveIniStr ('SearchStr', string(fReplace.edLookFor.Text));
  SaveIniStr ('ReplaceStr', string(fReplace.edReplaceWith.Text));

  if fReplace.edLookFor.Text = '' then
    fReplace.cbConfirmEach.checked := true;

  cancelled := false;
  doLocked := false;
  skipLocked := false;
  Count := 0;
  entCount := 0;
  mtxtcount := 0;
  if fReplace.rbCurrent.Checked then begin
    mode_init (mode);
    result := doReplace (mode)
  end
  else if fReplace.rbON.Checked then begin
    mode_init1 (mode);
    result := doReplace (mode);
  end
  else if fReplace.rbALL.Checked then begin
    mode_init (mode);
    mode_lyr (mode, lyr_all);
    result := doReplace (mode);
  end
  else if fReplace.rbSelect.Checked then begin
    result := true;
    for i := 0 to fReplace.ListBox1.Count-1 do
      if fReplace.ListBox1.Selected[i] then begin
        mode_init (mode);
        mode_1lyr (mode, TLyr(fReplace.ListBox1.Items.Objects[i]).Addr);
        SelectedLyr (TLyr(fReplace.ListBox1.Items.Objects[i]).Addr, true);
        if not doReplace (mode) then
          result := false;
      end
      else
        SelectedLyr (TLyr(fReplace.ListBox1.Items.Objects[i]).Addr, false);
  end
  else begin
    beep;
    result := true;
  end;
  if not result then begin
    messagedlg (Msgs[17] + sLineBreak +  Msgs[18] + sLineBreak +
                Msgs[19] + sLineBreak + Msgs[20], mtWarning, [mbOK], 0);
  end;

  if Count = 0 then
    msg := Msgs[21]          //No
  else
    msg := inttostr (Count);
  if Count = 1 then
    msg := msg + Msgs[22]     // replacement was
  else
    msg := msg + Msgs[23];    // replacements were
  msg := msg + Msgs[24];   // made
  if entCount < Count then
    msg := msg + Msgs[25] { in} + ' ' + inttostr (entCount) + Msgs[26] { entities.}
  else
    msg := msg + '.';
  if mtxtcount > 0 then
    msg := msg + sLineBreak + 'Search string found (but not replaced) in ' + inttostr (mtxtcount) + ' mtext/ptext entities';

end;


procedure TfReplace.About1Click(Sender: TObject);
begin
  fmAbout := TfmAbout.Create(fReplace);
  fmAbout.ShowModal;
  fmAbout.free;
end;

procedure TfReplace.btnProcessClick(Sender: TObject);
var
  msg : string;
begin
  ProcessReplace (msg);
  msg := msg + sLineBreak + Msgs[27];     //(display will update when you exit the macro)
  Messagedlg (msg, mtInformation, [mbOK], 0);
end;

procedure TfReplace.btnReplaceExitClick(Sender: TObject);
var
  msg : string;
begin
  if Length (edLookFor.Text) = 0 then
    beep
  else begin
    ProcessReplace(msg);
    Messagedlg (msg, mtInformation, [mbOK], 0);
    fReplace.Close;
  end;
end;

procedure TfReplace.btnExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfReplace.cbCaseSensitiveClick(Sender: TObject);
begin
  SaveBln ('dhTRCaseSens', cbCaseSensitive.Checked, true, true);
end;

procedure TfReplace.cbConfirmEachClick(Sender: TObject);
begin
  SaveBln ('dhTRConfirm', cbConfirmEach.Checked, true, true);
end;

procedure TfReplace.cbSortedClick(Sender: TObject);
var
  Selected : TList;
  i, j : integer;
begin
  Selected := TList.Create;
  if rbSelect.checked then begin
    for i := 0 to ListBox1.Count-1 do
      if fReplace.ListBox1.Selected[i] then
        Selected.Add(ListBox1.Items.Objects[i]);
  end;
  SaveSelected;
  ListBox1.Sorted := cbSorted.Checked;
  ListBox1.Items.Clear;
  PopulateLyrList;
  if rbSelect.checked then begin
    for i := 0 to Selected.Count-1 do begin
      for j := 0 to ListBox1.Count-1 do
        if LyrEqual (TLyr(ListBox1.Items.Objects[j]).Addr, TLyr(Selected[i]).Addr) then
          ListBox1.Selected[j] := true;
    end;
  end;
  Selected.Free;
  SaveBln ('LyrsSorted',  cbSorted.Checked, true, false);
end;

procedure TfReplace.cbWholeEntsClick(Sender: TObject);
begin
  SaveBln ('dhTRWholeEnt', cbWholeEnts.Checked, true, true);
end;

procedure TfReplace.cbWholeWordsClick(Sender: TObject);
begin
  SaveBln ('dhTRWholeWrd', cbWholeWords.Checked, true, true);
end;

procedure TfReplace.FormCreate(Sender: TObject);
var
  MyLyr : TLyr;     //class which simply holds layer address so that it can be added to the list as an object
  lyr     : lyraddr;
  lyrname : shortstring;
  ndx : integer;
  LyrSetting : string;
  rlyr : RLayer;
begin
  justStarting := true;
  if not InitSettings ('TextReplace') then begin
    PostMessage(Handle, WM_CLOSE, 0, 0);
    exit;
  end;

  LyrSetting := GetSvdStr ('dhTRLyrSelec', 'Current');
  if LyrSetting = 'Current' then
    rbCurrent.Checked := true
  else if LyrSetting = 'ON' then
    rbON.Checked := true
  else if LyrSetting = 'ALL' then
    rbALL.Checked := true
  else if LyrSetting = 'Select' then
    rbSelect.Checked := true;

  cbWholeWords.Checked := GetSvdBln ('dhTRWholeWrd', false);
  cbWholeEnts.Checked := GetSvdBln ('dhTRWholeEnt', false);
  cbCaseSensitive.Checked := GetSvdBln ('dhTRCaseSens', false);
  cbConfirmEach.Checked := GetSvdBln ('dhTRConfirm', false);

  cbSorted.Checked := GetIniBln ('LyrsSorted', true);

  ListBox1.Sorted := cbSorted.Checked;

  lyr := lyr_first;
  repeat
    MyLyr := TLyr.Create(lyr);
    getlyrname (lyr, lyrname);
    lyr_get (lyr, rlyr);
    if rlyr.group <> -1 then
      lyrname := lyrname + ' (LOCKED)';
    ndx := ListBox1.Items.AddObject(string(lyrname), MyLyr);
    if rbCurrent.Checked then
      ListBox1.Selected[ndx] := LyrEqual (lyr, GetLyrCurr)
    else if rbON.Checked then
      ListBox1.Selected[ndx] := Lyr_ison (lyr)
    else if rbALL.Checked then
      ListBox1.Selected[ndx] := true
    else if rbSelect.Checked then
      ListBox1.Selected[ndx] := IsSelectedLyr (lyr)
    else
      ListBox1.Selected[ndx] := false;
    lyr := lyr_next(lyr);
  until isnil (lyr);

  edLookFor.Text := GetIniStr ('SearchStr', '');
  edReplaceWith.Text := GetIniStr ('ReplaceStr', '');

  lblSearchFor.Caption := Msgs[0];
  lblReplaceWith.Caption := Msgs[1];
  lblLayers.Caption := Msgs[2];
  cbSorted.Caption := Msgs[3];
  rbCurrent.Caption := Msgs[4];
  rbON.Caption := Msgs[5];
  rbAll.Caption := Msgs[6];
  rbSelect.Caption := Msgs[7];
  cbWholeWords.Caption := Msgs[8];
  cbWholeEnts.Caption := Msgs[9];
  cbCaseSensitive.Caption := Msgs[10];
  cbConfirmEach.Caption := Msgs[11];
  btnProcess.Caption := Msgs[12];
  btnReplaceExit.Caption := Msgs[13];

  justStarting := false;
end;

procedure TfReplace.Hel1Click(Sender: TObject);
var
  filename : string;
  s : shortstring;
begin
  getpath(s, pathsup);
  filename := string(s) + 'dhsoftware\TextReplace.pdf';
  ShellExecute(self.WindowHandle,'open', PWIdeChar(filename),nil,nil, SW_SHOWNORMAL);
end;

procedure TfReplace.ListBox1Click(Sender: TObject);
begin
  rbSelect.Checked := true;
  SaveStr ('dhTRLyrSelec', 'Select', true);
end;

procedure TfReplace.rbALLClick(Sender: TObject);
begin
  ListBox1.SelectAll;
  SaveStr ('dhTRLyrSelec', 'ALL', true);
end;

procedure TfReplace.rbCurrentClick(Sender: TObject);
var
  i : integer;
begin
  for i := 0 to ListBox1.Count-1 do
    ListBox1.Selected[i] := LyrEqual (GetLyrCurr, TLyr(ListBox1.Items.Objects[i]).Addr);
  SaveStr ('dhTRLyrSelec', 'Current', true);
end;

procedure TfReplace.rbONClick(Sender: TObject);
var
  i : integer;
begin
  for i := 0 to ListBox1.Count-1 do
    ListBox1.Selected[i] := lyr_ison (TLyr(ListBox1.Items.Objects[i]).Addr);
  SaveStr ('dhTRLyrSelec', 'ON', true);
end;

procedure TfReplace.rbSelectClick(Sender: TObject);
begin
  SaveStr ('dhTRLyrSelec', 'Select', true);
end;

end.
