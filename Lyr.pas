unit Lyr;

{$M+}

interface
  uses URecords;

  type
    TLyr = Class(TObject)
       private
          MyAddr : lyrAddr;
       published
         constructor Create(Addr : lyraddr);
         property Addr : lyraddr
             read MyAddr;
    end;

implementation
 constructor TLyr.Create(Addr : lyraddr);
 begin
   MyAddr  := Addr;
 end;


end.