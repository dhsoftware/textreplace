unit Settings;

interface

uses inifiles, SysUtils, Dialogs, Controls, System.IOUtils, URecords, UInterfaces,
     UConstants, System.Classes, System.UITypes, Winapi.Windows;

var
  Msgs : TStringList;

PROCEDURE SaveIniInt (name : atrname;  value : integer);
PROCEDURE SaveInt (name : atrname;  value : integer; toini : boolean);
FUNCTION GetIniInt (name : atrname; default : integer) : integer;
FUNCTION GetSvdInt (name : atrname; default : integer) : integer;
FUNCTION InitSettings (MacroName : string) : boolean;
PROCEDURE SaveAng (name : atrname;  value : Double; toini : boolean);
FUNCTION GetIniRl (name : atrname; default : Double) : Double;
FUNCTION GetSvdAng (name : atrname; default : Double) : Double;
PROCEDURE SaveRl (name : atrname;  value : Double; toini : boolean);
PROCEDURE SaveIniRl (name : atrname; value : Double);
FUNCTION GetSvdRl (name : atrname; default : Double) : Double;
PROCEDURE SaveBln (name : atrname;  value : boolean; toini : boolean; toAttr : boolean);
FUNCTION GetIniBln (name : atrname; default : boolean) : boolean;
FUNCTION GetSvdBln (name : atrname; default : boolean) : boolean;
PROCEDURE SaveIniStr (name : atrname;  value : string);
PROCEDURE SaveStr (name : atrname;  value : string; toini : boolean);
FUNCTION GetIniStr (name : atrname; default : string) : string;
FUNCTION GetSvdStr (name : atrname; default : string) : string;
PROCEDURE SaveLayer (name : atrname; lyr : layer);
FUNCTION GetSvdLyr (name : atrname; VAR lyr : layer) : boolean;
FUNCTION readIniStr (iniFileName : string; section : string; ident : atrname;
                    default : string) : string;
PROCEDURE writeIniStr (iniFileName : string; section : string; ident : atrname; value : string);

implementation


VAR
	iniFileName	: string;
//	ident				: string;
	section			: string;
	atr					: attrib;
	initialised	: boolean;
  prompt      : string;

PROCEDURE writeIniInt (iniFileName : string; section : string; ident : atrname; value : integer);
VAR
  iniFile: TIniFile;
  id : string;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    id := string(ident);
    iniFile.WriteInteger(section, id, value);
  finally
    iniFile.Free;
  end;
END;

PROCEDURE writeIniStr (iniFileName : string; section : string; ident : atrname; value : string);
VAR
  iniFile: TIniFile;
  id : string;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    id := string(ident);
    iniFile.WriteString(section, id, value);
  finally
    iniFile.Free;
  end;
END;

PROCEDURE writeIniReal (iniFileName : string; section : string; ident : atrname; value : Double);
VAR
  iniFile: TIniFile;
  id : string;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    id := string(ident);
    iniFile.WriteFloat(section, id, value);
  finally
    iniFile.Free;
  end;
END;

FUNCTION readIniStr (iniFileName : string; section : string; ident : atrname;
                    default : string) : string;
VAR
  iniFile: TIniFile;
  id : string;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    id := string(ident);
    result := iniFile.ReadString(section, id, default) ;
  finally
    iniFile.Free;
  end;
END;

FUNCTION readIniInt (iniFileName : string; section : string; ident : atrname;
                    default : integer) : integer;
VAR
  iniFile: TIniFile;
  id : string;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    id := string(ident);
    result := iniFile.ReadInteger(section, id, default) ;
  finally
    iniFile.Free;
  end;
END;

FUNCTION readIniReal (iniFileName : string; section : string; ident : atrname;
                    default : Double) : Double;
VAR
  iniFile: TIniFile;
  id : string;
BEGIN
  iniFile := TIniFIle.Create(iniFIleName);
  try
    id := string(ident);
    result := iniFile.ReadFloat(section, id, default) ;
  finally
    iniFile.Free;
  end;
END;

FUNCTION InitSettings (macroName : string) :boolean;
VAR
	SupPath, MacroPath, MacroPath1, TempStr, OldIniFile : shortstring;
  filename : shortstring;
BEGIN
  result := true;
  try
    Msgs := TStringList.Create;
    uInterfaces.getpath(filename, pathsup);
    filename := filename + shortstring('dhsoftware\' + macroName + '.msg');
    Msgs.LoadFromFile(string(filename));
  except
    MessageDlg ('Error loading message file', mtError, [mbOK], 0);
    result := false;
    exit;
  end;

	section := macroName;
	uInterfaces.getpath (SupPath, pathsup);
	iniFileName := string(SupPath) + 'dhsoftware/dhsoftware.ini';
	uInterfaces.getpath (MacroPath, pathmcr);
	if not  fileexists (inifilename) then
		writeIniStr (iniFileName, section, 'PathMCR', string(MacroPath));

	MacroPath1 := shortstring(readIniStr (iniFileName, Section, 'PathMCR', ''));
	writeIniStr (iniFileName, section, 'PathMCR', string(MacroPath));

	if length (MacroPath1) = 0 then begin
		writeIniStr (iniFileName, section, 'PathMCR', string(MacroPath));
		MacroPath1 := MacroPath;
	end;

	iniFileName := string(MacroPath) + string(macroName) + '.ini';

	if CompareText (string(MacroPath), string(MacroPath1)) <> 0 then begin
		OldIniFile := MacroPath1 + shortstring(macroName) + '.ini';
		if FileExists (string(OldIniFile)) then begin
			prompt := Msgs[31] + sLineBreak;     //Settings for this macro are stored in a file in your macro path.
      prompt := prompt + Msgs[32] + sLineBreak + string(MacroPath1) + sLineBreak + sLineBreak;  //Last time you used the macro this path was:
			prompt := prompt + Msgs[33] + sLineBreak + string(MacroPath) + sLineBreak + sLineBreak;  //It is now:
			prompt := prompt + Msgs[34];  //Would you like to copy settings from the previous path to the current one?

      if messagedlg (prompt, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
				TempStr := shortstring(iniFileName + '-old');
				if FileExists (string(TempStr)) then
					DeleteFile (PWideChar(TempStr));
				RenameFile (iniFileName, string(TempStr));
				TFile.Copy (string(OldIniFile), iniFileName);
			end;
		end;
	end;
	initialised := true;
END;

PROCEDURE TxtSzSaveName (var str : string; ndx : integer);
BEGIN
	if ndx < 6 then begin
		str := 'dhSPStl' + inttostr (ndx) + 'Siz';
	end else if ndx = 6 then begin
		str := 'dhSPRepTlSiz';
	end else if ndx = 7 then begin
		str := 'dhSPRepHdSiz';
	end else if ndx = 8 then begin
		str := 'dhSPRepLiSiz';
	end else if ndx = 9 then begin
		str := 'dhSPRepToSiz';
	end else if ndx = 10 then begin
		str := 'dhSPRepSHSiz';
	end else if ndx = 11 then begin
		str := 'dhSPRepSTSiz';
	end else if ndx = 12 then begin
		str := 'dhSPTotSuSiz';
  end
	else begin
		beep (400, 100);
		Messagedlg ('Unrecognised Text Style Index.  Please report bug to macro author', mtWarning, [mbOK], 0);
	end;
END; //TxtSzSaveName;

PROCEDURE SaveIniInt (name : atrname;  value : integer);
BEGIN
		writeIniInt (iniFileName, section, name, value);
END;

PROCEDURE SaveInt (name : atrname;  value : integer; toini : boolean);
BEGIN
	if atr_sysfind (name, atr) then begin
		atr.int := value;
		atr_update (atr);
  end
	else begin
		atr_init (atr, atr_int);
		atr.name := name;
		atr.int := value;
		atr_add2sys (atr);
	end;
	if toini then
		SaveIniInt(name, value);
END;

FUNCTION GetIniInt (name : atrname; default : integer) : integer;
BEGIN
		result := readIniInt (iniFileName, section, name, default);
END;

FUNCTION GetSvdInt (name : atrname; default : integer) : integer;
BEGIN
	if atr_sysfind (name, atr) then
		result := atr.int
	else
		result := GetIniInt (name, default);

END;

PROCEDURE SaveAng (name : atrname;  value : Double; toini : boolean);
BEGIN
	if atr_sysfind (name, atr) then begin
		atr.ang := value;
		atr_update (atr);
  end
	else  begin
		atr_init (atr, atr_ang);
		atr.name := name;
		atr.ang := value;
		atr_add2sys (atr);
	end;
	if toini then
		writeIniReal (iniFileName, section, name, value);
END;

FUNCTION GetIniRl (name : atrname; default : Double) : Double;
BEGIN
	result := readIniReal (iniFileName, section, name, default);
END;


FUNCTION GetSvdAng (name : atrname; default : Double) : Double;
BEGIN
	if atr_sysfind (name, atr) then
		result := atr.ang
	else
		result := GetIniRl (name, default);
END;

PROCEDURE SaveIniRl (name : atrname; value : Double);
BEGIN
		writeIniReal (iniFileName, section, name, value);
END;


PROCEDURE SaveRl (name : atrname;  value : Double; toini : boolean);
BEGIN
	if atr_sysfind (name, atr) then begin
		atr.rl := value;
		atr_update (atr);
  end
	else begin
		atr_init (atr, atr_rl);
		atr.name := name;
		atr.rl := value;
		atr_add2sys (atr);
	end;
	if toini then
		SaveIniRl (name, value);
END;

FUNCTION GetSvdRl (name : atrname; default : Double) : Double;
BEGIN
	if atr_sysfind (name, atr) then
		result := atr.rl
	else
		result := GetIniRl (name, default);
END;




PROCEDURE SaveBln (name : atrname;  value : boolean; toini : boolean; toAttr : boolean);
VAr
	ival : integer;
	sval : str255;
BEGIN
	if value then begin
		ival := 1;
		sval := 'true';
  end
	else begin
		sval := 'false';
		ival := 0;
	end;
	if toAttr then begin
		if atr_sysfind (name, atr) then begin
			atr.int := ival;
			atr_update (atr);
    end
		else begin
			atr_init (atr, atr_int);
			atr.name := name;
			atr.int := ival;
			atr_add2sys (atr);
		end;
	end;
	if toini then
		writeIniStr (iniFileName, section, name, string(sval));
END;

FUNCTION GetIniBln (name : atrname; default : boolean) : boolean;
VAR
	s : string;
	d	: string;
BEGIN
	if default then
		d := 'true'
	else
		d := 'false';

	s := readIniStr (iniFileName, section, name, d);
	result := (pos ('t', s, 1) = 1) or (pos ('T', s, 1) = 1);
END;

FUNCTION GetSvdBln (name : atrname; default : boolean) : boolean;
BEGIN
	if atr_sysfind (name, atr) then
    result := atr.int <> 0
	else
		result := GetIniBln (name, default);
END;

PROCEDURE SaveIniStr (name : atrname;  value : string);
BEGIN
	writeIniStr (iniFileName, section, name, value);
END;


PROCEDURE SaveStr (name : atrname;  value : string; toini : boolean);
BEGIN
	if atr_sysfind (name, atr) then begin
		atr.str := str80(value);
		atr_update (atr);
  end
	else begin
		atr_init (atr, atr_str);
		atr.name := name;
		atr.str := str80(value);
		atr_add2sys (atr);
	end;
	if toini then
		SaveIniStr (name, value);
END;

FUNCTION GetIniStr (name : atrname; default : string) : string;
BEGIN
	result := readIniStr (iniFileName, section, name, default);
END;

FUNCTION GetSvdStr (name : atrname; default : string) : string;
BEGIN
	if atr_sysfind (name, atr) then
		result := string(atr.str)
	else
		result := GetIniStr (name, default);
END;

PROCEDURE SaveLayer (name : atrname; lyr : layer);
VAR
	templyr : layer;
BEGIN
	templyr := lyr_first;
	while not lyr_nil (templyr) do begin
		if atr_lyrfind (templyr, name, atr) then begin
			if (templyr.page <> lyr.page) or (templyr.ofs <> lyr.ofs) then
				atr_dellyr (templyr, atr)
    end
		else if (templyr.page = lyr.page) and (templyr.ofs = lyr.ofs) then begin
			atr_init (atr, atr_str);
			atr.name := name;
			atr.str := name;
			atr_add2lyr (lyr, atr);
		end;
		templyr := lyr_next(templyr);
	end;
END;

FUNCTION GetSvdLyr (name : atrname; VAR lyr : layer) : boolean;
VAR
	TempLyr : layer;
BEGIN
	templyr := lyr_first;
	while not lyr_nil (templyr) do begin
		if atr_lyrfind (templyr, name, atr) then begin
			lyr := templyr;
			result := true;
      exit;
    end;
		templyr := lyr_next(templyr);
	end;
	result := false;
END;

END.

